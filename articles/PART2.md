This post is the second part in a two-part article on managing Javascript and CSS dependencies within a multiple-page application written in ASP.NET Core MVC.
In [the first part](https://dev.to/larswillemsens/managing-asp-net-core-mvc-front-end-dependencies-with-npm-and-webpack-part-1-3jf5) we’ve specified our front-end dependencies, bumped the version numbers and set up a webpack build system.
In this part we’ll be tackling performance issues and we’ll make sure that the entire project (front-end _and_ back-end) can be built using a single command.

## Performance
While everything seems to be working ok, there are a few improvements to be made. When fast clicking between pages (i.e., 'Home' and 'Privacy') you might notice that CSS gets applied _after_ the page is rendered by the browser. That’s because the npm package `style-loader` plugs the CSS into the page after it was loaded causing the browser to re-render the page!
On top of that, the Javascript bundle — which contains the CSS — is very large. It contains the entire Bootstrap CSS as well as some Bootstrap Javascript functions and _all_ of JQuery!

Let’s take care of this. Our aim is the following:
* Bundling CSS into a separate CSS file that can be
  statically referenced from an HTML `link` tag
* Splitting up the Javascript code into separate
  bundles. Many pages have Javascript code that is unique to them and not every page needs JQuery

We’re only keeping JQuery around for two reasons:
* Some fancy Bootstrap components use it. [Find out here which ones are](https://getbootstrap.com/docs/4.6/getting-started/introduction/#js)
* ASP.NET Core’s client-side form validation uses it

To make sure that the solution that I’m about to present here fits all use cases, let’s set up a simple prototype. These are the pages we’re going to use:

![Alt Text](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/kvziau6f4q2x0g9k7v8d.png)

All of the pages include the site’s CSS as well as a bit of Javascript that is common to all pages. The individual pages are:
* The **Index** page, which has a bit of custom
  Javascript code running on it (`index.js`)
* The **Privacy** page, which has a Bootstrap component
  on it that is so fancy that it needs JQuery to function.
  It references Bootstrap’s Javascript code, which in
  turn references JQuery. (`bootstrap_js.js` - not to be
  confused with Bootstrap’s CSS, which is used everywhere
  on the site)
* The **Contact** page, which has a form on it that is
  backed by ASP.NET Core’s form validation. Validation
  can be done both server-side and client-side
  (`validation.js`)

With all of this in the pipeline you might be wondering why we went down this road in the first place. We will end up with individual CSS/JS files that are hard-referenced from the HTML pages… that’s what we started with! Well, sort of, but not quite. Here’s what’s different:
* We’re referencing our libraries with a specific version number
* The dependencies are not placed _inside_ the project tree
* We’ll end up with a performance gain (in the standard MVC template all of Bootstrap and JQuery are referenced from all pages)
* Our build system is extensible: Want Sass? No problem! Want to use the very latest ECMAScript features? You got it! Need minification or obfuscation? No problemo!

Just imagine what it would be like if all of this was already present in the standard MVC template. Then you’d have all of this modern front-end goodness without having to set it up yourself. Ha!

Ok, let’s go.

## Splitting up the bundle
This is the current state of things:
```
$ ll wwwroot/dist/
total 2984
-rw-rw-r-- 1 lars lars 1346282  9 apr 12:04 site.entry.js
-rw-rw-r-- 1 lars lars 1704977  9 apr 12:04 site.entry.js.map
```

That’s over 1300K worth of Javascript and CSS code.

These are the separate blocks that we had identified in the diagram above:
* Sitewide CSS (Bootstrap CSS and custom CSS)
* Sitewide Javascript
* Bootstrap’s JS code (which includes _all_ of JQuery):
  for fancy popup buttons and the like
* Validation scripts (basically JQuery with some extras):
  for forms that use ASP.NET Core’s form validation
* A sample Javascript code block that is unique to a
  specific page (let’s take ‘Home’, so `index.js`)

This is what our view files look like after we move all of the common parts into `_Layout.cshtml`:

![Alt Text](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/c1ykqi6hbnxpk69528km.png)

To split things up we’ll dive into `ClientApp/src/js/` and turn `site.js` into four files:
* `site.js`: Javascript and CSS code that is needed
  on each page (currently contains Bootstrap CSS and
  custom CSS). Will result in a **separate** JS and CSS
  file as seen in the diagram
* `bootstrap_js.js`: Bootstrap’s **Javascript** code
* `validation.js`: JQuery, including the validation scripts
  for our forms
* `index.js`: some dummy code that’s only applicable to ‘Home’

Here’s what they look like:

#### site.js
This file lost a few lines when compared to the previous version. CSS is needed on every page of the application so we’re including all of our CSS here:
```javascript
import 'bootstrap/dist/css/bootstrap.css';

// Custom CSS imports
import '../css/site.css';

console.log('The \'site\' bundle has been loaded!');
```

#### bootstrap_js.js
Here, we’re including bootstrap’s Javascript code. If an import statement doesn’t include a file extension, then it’s a JS file:
```javascript
import 'bootstrap';

console.log('The \'bootstrap_js\' bundle has been loaded!');
```

#### validation.js
These import lines were previously in `site.js`. We’re putting them into their own file so that they can be included separately:
```javascript
import 'jquery';
import 'jquery-validation';
import 'jquery-validation-unobtrusive';

console.log('The \'validation\' bundle has been loaded!');
```

#### index.js
… some dummy code:
```javascript
console.log('The \'index\' bundle has been loaded!');
```

### Configuring the webpack build
Separate files means separate [entries in webpack](https://webpack.js.org/guides/code-splitting/). Each entry is handled as a separate module and will result in a separate Javascript file. The resulting file for each entry will be named after the entry followed by the `.entry.js` suffix.
While we’re at it we’ll extract the CSS out of the Javascript bundle. Instead of using the `style-loader` npm package we’ll use `mini-css-extract-plugin`, which takes care of the extraction.

Brace yourself, `webpack.config.js` is coming…

```diff
 const path = require('path');
+const MiniCssExtractPlugin = require("mini-css-extract-plugin");

 module.exports = {
     entry: {
-        site: './src/js/site.js'
+        site: './src/js/site.js',
+        bootstrap_js: './src/js/bootstrap_js.js',
+        validation: './src/js/validation.js',
+        index: './src/js/index.js'
     },
     output: {
         filename: '[name].entry.js',
         path: path.resolve(__dirname, '..', 'wwwroot', 'dist')
     },
     devtool: 'source-map',
     mode: 'development',
     module: {
         rules: [
-            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
+            { test: /\.css$/, use: [{ loader: MiniCssExtractPlugin.loader }, 'css-loader'] },
             { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: ['file-loader'] },
             {
                 test: /\.(woff|woff2)$/, use: [
                     {
                         loader: 'url-loader',
                         options: {
                             limit: 5000,
                         },
                     },
                 ]
             },
             {
                 test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [
                     {
                         loader: 'url-loader',
                         options: {
                             limit: 10000,
                             mimetype: 'application/octet-stream',
                         },
                     },
                 ]
             },
             {
                 test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: [
                     {
                         loader: 'url-loader',
                         options: {
                             limit: 10000,
                             mimetype: 'image/svg+xml',
                         },
                     },
                 ]
             },
         ]
-    }
+    },
+    plugins: [
+        new MiniCssExtractPlugin({
+            filename: "[name].css"
+        })
+    ]
 };
```

At the very top and the very bottom you can see we’re importing an npm packages and adding it as a plugin respectively. Most plugins have a wide range of configuration options, but we only need to specify which filename to use for CSS files (`[name].css`).

In the `entry` section the different entries are defined and near the center of the file we’ve replaced `style-loader` with the plugin.

So one npm package is being replaced by another. Update `package.json` accordingly:
```diff
 {
     "name": "NetCoreNpmWebpack",
     "description": "ASP.NET Core MVC project with npm and webpack front-end configuration.",
     "repository": "https://gitlab.com/kdg-ti/integratieproject-1/guides/netcorenpmwebpack",
     "license": "MIT",
     "version": "3.0.0",
     "dependencies": {
         "jquery": "^3.6.0",
         "jquery-validation": "^1.19.3",
         "jquery-validation-unobtrusive": "^3.2.12",
         "bootstrap": "^4.6.0",
         "popper.js": "^1.16.1"
     },
     "devDependencies": {
         "webpack": "^5.31.0",
         "webpack-cli": "^4.6.0",
         "css-loader": "^5.2.0",
-        "style-loader": "^2.0.0",
+        "mini-css-extract-plugin": "^1.4.1",
         "file-loader": "^6.2.0",
         "url-loader": "^4.1.1"
     },
     "scripts": {
         "build": "webpack"
     }
 }
```

Let’s generate those new bundles. From the `ClientApp` directory, enter:

```
$ npm install
$ npm run build
```

Which build artifacts were generated this time?

```
$ ll ../wwwroot/dist/
total 2700
-rw-rw-r-- 1 lars lars 526226  9 apr 12:57 bootstrap_js.entry.js
-rw-rw-r-- 1 lars lars 656278  9 apr 12:57 bootstrap_js.entry.js.map
-rw-rw-r-- 1 lars lars    269  9 apr 12:57 index.entry.js
-rw-rw-r-- 1 lars lars    227  9 apr 12:57 index.entry.js.map
-rw-rw-r-- 1 lars lars 200669  9 apr 12:57 site.css
-rw-rw-r-- 1 lars lars 516978  9 apr 12:57 site.css.map
-rw-rw-r-- 1 lars lars   3140  9 apr 12:57 site.entry.js
-rw-rw-r-- 1 lars lars   1886  9 apr 12:57 site.entry.js.map
-rw-rw-r-- 1 lars lars 365375  9 apr 12:57 validation.entry.js
-rw-rw-r-- 1 lars lars 470353  9 apr 12:57 validation.entry.js.map
```

### The Views
After splitting up the bundle into multiple smaller bundles we now have to review our `link` and `script` tags.

With `mini-css-extract-plugin` in the picture, the CSS will have to be imported statically. CSS is used everywhere so we jump into `_Layout.cshtml`:

```diff
     ...

     <title>@ViewData["Title"] - NetCoreNpmWebpack</title>
     <script type="module" src="~/dist/site.entry.js" defer></script>
+    <link rel="stylesheet" href="~/dist/site.css">
 </head>
 <body>
     <header>

     ...
```

The `Home/Index.cshtml` page has custom Javascript:

```diff
 @{
     ViewData["Title"] = "Home Page";
 }

+@section Scripts
+{
+    <script type="module" src="~/dist/index.entry.js" defer></script>
+}
+
 <div class="text-center">

 ...
```

The `Privacy.cshtml` page gets a fancy Bootstrap component. Let’s pick a [dropdown menu button](https://getbootstrap.com/docs/4.6/components/dropdowns/#single-button)!

```diff
 @{
     ViewData["Title"] = "Privacy Policy";
 }
+
+@section Scripts
+{
+    <script type="module" src="~/dist/bootstrap_js.entry.js" defer></script>
+}
+
 <h1>@ViewData["Title"]</h1>

 <p>Use this page to detail your site's privacy policy.</p>
+
+<div class="dropdown">
+    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
+        Dropdown button
+    </button>
+    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
+        <a class="dropdown-item" href="#">Action</a>
+        <a class="dropdown-item" href="#">Another action</a>
+        <a class="dropdown-item" href="#">Something else here</a>
+    </div>
+</div>
```

… and then there’s the Contact page, a new page that we’ll build from scratch just to test form validation. We’ll need a **view**, a **view-model**, some new **actions** in the controller and a **link** on the site’s navigation bar.

Let’s start with the form itself, a new view in `Home/Contact.cshtml`:

```html
@model ContactViewModel

@{
    ViewBag.Title = "Contact";
    Layout = "_Layout";
}

@section Scripts
{
    <script type="module" src="~/dist/validation.entry.js" defer></script>
}

<h1>@ViewBag.Title</h1>

<form asp-controller="Home" asp-action="Contact">
    <div class="form-group">
        <label asp-for="Subject"></label>
        <input asp-for="Subject" class="form-control"/>
        <span asp-validation-for="Subject" class="text-danger"></span>
    </div>

    <div class="form-group">
        <label asp-for="Message"></label>
        <textarea asp-for="Message" class="form-control"></textarea>
        <span asp-validation-for="Message" class="text-danger"></span>
    </div>

    <button class="btn btn-primary" type="submit">Submit</button>
</form>
```

`Subject`, `Message`, `ContactViewModel`, … what are you on about!?
Let’s move out of the `Views` directory and into `Models`…
`ContactViewModel.cs`:

```csharp
using System.ComponentModel.DataAnnotations;

namespace NetCoreNpmWebpack.Models
{
    public class ContactViewModel
    {
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Please enter a message.")]
        public string Message { get; set; }
    }
}
```

… but the form is GET-ing and POST-ing all over the place, how is that handled?
Time to edit `HomeController`:

```diff
         ...

+        [HttpGet]
+        public IActionResult Contact()
+        {
+            return View();
+        }
+
+        [HttpPost]
+        public IActionResult Contact(ContactViewModel contactVM)
+        {
+            if (ModelState.IsValid)
+            {
+                // Send an email or save the message in a table...
+                // Redirect to a page that says "Thanks for contacting us!"...
+
+                return RedirectToAction("Index");
+             }
+
+            return View();
+        }
        
        ...
```

… ok and the link?
Back to `_Layout.cshtml`:

```diff
 ...

                         <li class="nav-item">
                             <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Privacy">Privacy</a>
                         </li>
+                        <li class="nav-item">
+                            <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Contact">Contact</a>
+                        </li>
                     </ul>
                 </div>
             </div>

 ...
```

Done! (almost)
We’ve now got a full-blown webpack and NPM powered front-end with excellent performance and modern Javascript goodness.

We don’t need `_ValidationscriptsPartial.cshtml` anymore so be sure to remove that one from your repository:

```
$ rm Views/Shared/_ValidationScriptsPartial.cshtml
```

If you’re consistent about adding `defer` to your script tags (and you should be! :)) then you can go one step further and **move** the `Scripts` section inside `_Layout` to that page’s `head` section.

```diff
 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <title>@ViewData["Title"] - NetCoreNpmWebpack</title>
     <script type="module" src="~/dist/site.entry.js" defer></script>
+    @await RenderSectionAsync("Scripts", required: false)
     <link rel="stylesheet" href="~/dist/site.css">
 </head>
 <body>

     ...

     <footer class="border-top footer text-muted">
         <div class="container">
             &copy; 2021 - NetCoreNpmWebpack - <a asp-area="" asp-controller="Home" asp-action="Privacy">Privacy</a>
         </div>
     </footer>
-    @await RenderSectionAsync("Scripts", required: false)
 </body>
 </html>
```

The project so far can be found on GitLab as [version 3](https://gitlab.com/kdg-ti/integratieproject-1/guides/netcorenpmwebpack/-/tags/version_3) of [NetCoreNpmWebpack](https://gitlab.com/kdg-ti/integratieproject-1/guides/netcorenpmwebpack).
Give it a spin. You’ll notice that performance is good.

**Note:** We're currently only including the `bootstrap_js.entry.js` file on the page that contains the dropdown. This will, unfortunately, mess with our responsiveness. When we're on an extra small screen, a hamburger menu will be shown which requires Bootstrap. So if you care about responsiveness, you'll be better off importing the Bootstrap JavaScript code from `site.js` and removing `bootstrap_js.js` altogether.

## Building the project
Running the project is perhaps easier said than done. Let’s recap:

``` bash
$ npm install          # only after a modification to package.json
$ npm run build
$ dotnet build
$ dotnet run
```

That’s too much typing for anyone, let’s automate that a bit.

The `.csproj` file can be extended with some extra build commands. Honestly, csproj-Hocus Pocus is a bit of uncharted territory for me (although it reminds me of the [Ant build system](https://en.wikipedia.org/wiki/Apache_Ant)), but this seems to work fine:

```diff
 <Project Sdk="Microsoft.NET.Sdk.Web">

     <PropertyGroup>
         <TargetFramework>net5.0</TargetFramework>
+        <IsPackable>false</IsPackable>
+        <MpaRoot>ClientApp\</MpaRoot>
+        <WWWRoot>wwwroot\</WWWRoot>
+        <DefaultItemExcludes>$(DefaultItemExcludes);$(MpaRoot)node_modules\**</DefaultItemExcludes>
     </PropertyGroup>
+
+    <ItemGroup>
+        <PackageReference Include="Microsoft.AspNetCore.SpaServices.Extensions" Version="5.0.4"/>
+    </ItemGroup>
+
+    <ItemGroup>
+        <!-- Don't publish the MPA source files, but do show them in the project files list -->
+        <Content Remove="$(MpaRoot)**"/>
+        <None Remove="$(MpaRoot)**"/>
+        <None Include="$(MpaRoot)**" Exclude="$(MpaRoot)node_modules\**"/>
+    </ItemGroup>
+
+    <Target Name="NpmInstall" BeforeTargets="Build" Condition=" '$(Configuration)' == 'Debug' And !Exists('$(MpaRoot)node_modules') ">
+        <!-- Ensure Node.js is installed -->
+        <Exec Command="node --version" ContinueOnError="true">
+            <Output TaskParameter="ExitCode" PropertyName="ErrorCode"/>
+        </Exec>
+        <Error Condition="'$(ErrorCode)' != '0'" Text="Node.js is required to build and run this project. To continue, please install Node.js from https://nodejs.org/, and then restart your command prompt or IDE."/>
+        <Message Importance="high" Text="Restoring dependencies using 'npm'. This may take several minutes..."/>
+        <Exec WorkingDirectory="$(MpaRoot)" Command="npm install"/>
+    </Target>
+
+    <Target Name="NpmRunBuild" BeforeTargets="Build" DependsOnTargets="NpmInstall">
+        <Exec WorkingDirectory="$(MpaRoot)" Command="npm run build"/>
+    </Target>
+
+    <Target Name="PublishRunWebpack" AfterTargets="ComputeFilesToPublish">
+        <!-- As part of publishing, ensure the JS resources are freshly built in production mode -->
+        <Exec WorkingDirectory="$(MpaRoot)" Command="npm install"/>
+        <Exec WorkingDirectory="$(MpaRoot)" Command="npm run build"/>
+
+        <!-- Include the newly-built files in the publish output -->
+        <ItemGroup>
+            <DistFiles Include="$(WWWRoot)dist\**"/>
+            <ResolvedFileToPublish Include="@(DistFiles->'%(FullPath)')" Exclude="@(ResolvedFileToPublish)">
+                <RelativePath>%(DistFiles.Identity)</RelativePath>
+                <CopyToPublishDirectory>PreserveNewest</CopyToPublishDirectory>
+                <ExcludeFromSingleFile>true</ExcludeFromSingleFile>
+            </ResolvedFileToPublish>
+        </ItemGroup>
+    </Target>
+
+    <Target Name="NpmClean" BeforeTargets="Clean">
+        <RemoveDir Directories="$(WWWRoot)dist"/>
+        <RemoveDir Directories="$(MpaRoot)node_modules"/>
+    </Target>

 </Project>
```

As you may have figured out from the diff above, the `npm install` command is _only_ executed in case the `node_modules` directory is absent. That’s something to keep in mind in case you make modifications to `package.json`!

Now we can build the project in its entirety using the `dotnet build` command. Excellent! (pressing the run or compile button from your IDE works just as well)

### Auto-building the bundle
To make life even easier, we want to automagically rebuild the bundle whenever the front-end code changes. At the same time we _don’t_ want to restart .NET Core’s HTTP server (Kestrel) when that happens.

To make this happen, we’ll add a webpack watcher for the front-end files that will trigger a rebuild. In `package.json`:

```diff
 ...
         "url-loader": "^4.1.1"
     },
     "scripts": {
-        "build": "webpack"
+        "build": "webpack",
+        "watch": "webpack --watch"
     }
 }
```

While editing front-end code our workflow will look like this:
1. `npm run watch` (executed from within the `ClientApp` directory)
2. `dotnet run`

(Note: I would advise **against** using `dotnet watch` since it seems to continuously detect changes to the bundle causing an endless rebuild loop)

[Version 4 of the sample project can be found here.](https://gitlab.com/kdg-ti/integratieproject-1/guides/netcorenpmwebpack/-/tags/version_4)

## Wrapping up
We now have a flexible and extensible project that is using modern front-end technologies and has excellent performance.

We’ve had to cover quite a bit of ground since many of these techniques are absent in most tutorials. Bower, Grunt and Gulp were dominant just a few years ago, but are now on their decline. Many sources on the internet still refer to these kings of yesteryear. However, on Bower’s website you can see that they are actively recommending alternatives.

I think that this guide may have filled a gap by bringing npm and webpack into MVC and MPA applications, more specifically .NET Core apps.

### What’s left?
There is no distinction yet between “Development” and “Production”. Minification of JavaScript code as well as CSS pruning are still to be added. I’m confident, though, that the flexibility of the build system won’t make that too challenging.

If you have any other suggestions, then please let me know in the comments below.

Good luck building your MVC application!
